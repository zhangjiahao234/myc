#include<stdio.h>

int add(int a,int b){
	return a+b;
}


int sub(int a,int b){
	return a-b;
}
int mul(int a,int b){
	return a*b;
}
int max(int a,int b){
	return a>b?a:b;
}


void process(int a,int b, int(*f)(int ,int)){

	int result;
	result=(*f)(a,b);
	printf("%d\n",result);
}


int main(){
int a=5;
int b=10;
process(a,b,max);
process(a,b,add);
process(a,b,mul);
process(a,b,sub);


return 0;
}
