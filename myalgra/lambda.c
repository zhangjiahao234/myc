/*
 * 不支持g++和clang
 * 
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// 定义lambda宏
#define lambda(return_type, function_body)\
({\
        return_type $this function_body\
        $this;\
})
#define $ lambda


#define add2int(function_body) $(int,(int _a,int _b){function_body})

int sum(int *arr, int length, int (*add)(int, int));


int sum(int *arr, int length, int (*add)(int, int)) {
        int i = 0,sum = 0;
        for (i = 0; i < length; i++) {
                sum = add(sum, arr[i]);
        }
        return sum;
}

void hi(char str[], void (*print)(char str[])) {
        print(str);                                                             
}

int main(int argc, char *argv[]) {

    	
        int arr[] = {1,2,3,4,5};
        //int ret = sum(arr, sizeof(arr)/sizeof(int), add2int(int c = _a + _b; return c;));
        //通过匿名函数对一个数组求和
    	int ret = sum(arr, sizeof(arr)/sizeof(int), $(int, (int _a, int _b){
                int c = _a + _b;
                return c;
        }));
        printf("sum of arr is %d\n", ret);

    	//定义一个求x*y的匿名函数，并将其函数指针赋给hello
        int (*hello)(int, int) = $(int, (int _y, int _w) {
                return _y * _w;
        });

        int res = hello(2,3);
        printf("res: %d\n", res);

        //直接使用lambda表达式输出字符串
        hi("hahaha", $(void, (char str[]){
                printf("%s\n", str);
        }));
        /* 
        //和上面功能一样
        void (*print)(char str[]) = $(void, (char str[]){
                printf("%s\n", str);
        });
        hi("hahaha", print);
        */
        exit(0);
}