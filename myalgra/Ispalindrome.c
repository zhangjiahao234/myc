#include<stdio.h>

typedef int bool;
# define true 1;
# define false 0;

/*
typedef int bool;
enum{false,true};
*/

//typedef enum{false,true}bool;

bool isPalindrome(const char*s ,int n){

	if(s==NULL || n<1){
	return false;
	}
	const char* font,* back;
  font=s;
  back=s+n-1;
  while(font<back){
	if(*font!=*back){
		return false;
	}
  ++font;
	--back;
}
return true;
}

int main(){
char s[]="abcbadddd";
printf("%d",isPalindrome(s,9));


return 0;
}
