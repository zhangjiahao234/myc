#include<stdio.h>

void swap(int* a,int* b){
	int tmp;
	tmp=*a;
	*a=*b;
	*b=tmp;
}

// yinyong call must be in cpp and swap(int &a, int & b);swap(5,3);

int main(){
  int a=2;
	int b=3;
  swap(&a,&b);
  printf("%d,%d",a,b);
	return 0;
}
