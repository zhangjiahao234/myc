//dir is the file path and set para1 : DB path + tabl
//dir set .Q.en[`:/mnt/kdb_data2/FeeRateDB/DB] update `g#Product from `Exch`Product xasc tb;

DIR: `:/home/data/zjh/myc/qgod/1;

dirs:((`$3 cut -5 _ .Q.A ),`$-5#.Q.A)!hsym each `$read0 ` sv DIR, `par.txt;


// make a mapping
// make the colums begin with A,B,C to the partion ABC,
// make the colums begin with all smaller to the partion VWXYZ.
getpart:.Q.fu{
    [symlist]
    key[dirs] 0 3 6 9 12 15 18 21 bin .Q.A?first each string symlist,() }
//` sv dirs[`ABC],(`$string 2016.11.23),`tablename,`
//return `:/data/0/2016.11.23/tablename/
saveOnepart:{
    [dt;tablename;data;part2save]
    (` sv dirs[part2save],(`$string dt),tablename,`)set
    .Q.en[DIR] 
    delete part from select from data where part=part2save;
    }
    
CHUNK: update part: getpart underlyingSym from CHUNK;
saveOnepart[DATE;`QUOTE;CHUNK]each distinct exec part from CHUNK;

//function applies p# attribute to sym and underlyingSym Columns 
//of the quote table for the specified date and directory
addphashes:{[dt;dir]  {[dt;dir;f]@[` sv dir,(`$string dt),`QUOTE;f;`p#]}[dt;dir]each   `sym`underlyingSym} 


//addphashes[DATE] each value dirs;


//add links to the market data

dirs: `$read0 ` sv DIR,`par.txt ;


addlinks:{[dt;dir] 
    dir:` sv dir,`$string dt;  
    // compute links as an asof join.
    inds:select ind: x from  
       aj[`sym`timestamp; 
       select sym:underlyingSym,timestamp from dir`QUOTE;  
       select sym,timestamp,i from dir`EQUOTE];  
       // save the links
    (` sv dir,`QUOTE`underlying)set `EQUOTE!exec ind from inds;  
       // update the metadata of the QUOTE table
     u set distinct get[u:` sv dir,`QUOTE`.d],`underlying;
 } 
